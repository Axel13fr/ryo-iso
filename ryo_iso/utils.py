"""Utility functions."""
import os as _os
import subprocess as _subprocess
import time as _time
from pathlib import Path as _Path

# FIXME: cleanup
# import re as _re
# import shutil as _shutil
# import sys as _sys
# import appdirs as _appdirs
# import delegator as _delegator
# NOTE: importlib backport for Python 3.5
# import importlib_resources as _resources
# import requests as _requests
# import yaml as _yaml
from loguru import logger as _logger


def cleanup_data():
    """
    Clean up build artifacts.

    Removes ['./squashfs-root','./image','./build']

    .. warning:: Do not manually ``rm -rf squashfs-root`` if the chrooted devices are mounted
    """
    paths = [
        "./squashfs-root",
        "./image",
        "./build",
        "./base_image.mbr",
        "./base_image.efi",
    ]
    paths = map(_Path, paths)
    paths = filter(lambda path: path.exists(), paths)
    for path in paths:
        p = _subprocess.run(["sudo", "rm", "--one-file-system", "--recursive", str(path)])
        if p.returncode != 0:
            _logger.debug("rm failed" + repr(p))
        else:
            _logger.debug("rm: %s" % str(path))
            _logger.debug(repr(p))


def umount_dev():
    """
    Unmount chrooted pseudo-filesystems.

    umount ['proc/sys/fs/binfmt_misc', 'proc', 'run/dbus', 'sys', 'dev/pts', 'dev'] in the chroot
    """
    _logger.info("Umounting chroot pseudo-filesystems")

    devices = ["proc/sys/fs/binfmt_misc", "proc", "run/dbus", "sys", "dev/pts", "dev"]
    # FIXME: C417 Unnecessary use of map - use a generator expression instead.
    devices = map(lambda device: "squashfs-root/" + device, devices)  # noqa
    mounted = list(filter(_os.path.ismount, devices))

    if len(mounted) == 0:
        _logger.debug("No pseudo-filesystems mounted in chroot")
    else:
        for device in mounted:
            p = _subprocess.run(["sudo", "umount", device])
            if p.returncode != 0:
                _logger.debug("umount failed" + repr(p))
            else:
                _logger.debug("umount: %s" % device)


def profile_start():
    """
    Start pydoit profiler.

    This `python-action <https://pydoit.org/tasks.html#python-action>`_
    returns a dictionary with the profiler start time, which is made
    available to other actions in the same Task instance

    Returns
    -------
    dict
        'start': start time of the profiler
    """
    return {"start": _time.time()}


def profile_stop(task, **kwargs):
    """
    Stop pydoit profiler.

    This `python-action <https://pydoit.org/tasks.html#python-action>`_
    accesses the start time from the :func:`profile_start` action

    Parameters
    ----------
    task: dict
        Access to start time via pydoit Task instance
    **kwargs: dict
        Keyword argument access to start time

    Returns
    -------
    dict
        'duration': profiler duration
    """
    if "start" in kwargs:
        duration = _time.time() - kwargs["start"]
    else:
        duration = _time.time() - task.values["start"]
    _logger.debug(task.name + ": " + str(duration) + " seconds")
    return {"duration": duration}
