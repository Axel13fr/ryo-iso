---
# CI for `ryo-iso`
# SPDX-FileCopyrightText: 2023 HXR LLC <code@hxr.io>
# SPDX-License-Identifier: Apache-2.0 OR MIT

default:
  tags:
    - docker

variables:
  GIT_SUBMODULE_STRATEGY: recursive
  DEBEMAIL: code@hxr.io
  DEBFULLNAME: HXR CI
  DEBIAN_FRONTEND: noninteractive
  TERM: xterm-256color

stages:
  - test
  - deploy
  - integration

######## #### ####  ####   ####     ####        ####             ####
# Static Analyzers - Format & Lint
########

test-static:
  image: registry.gitlab.com/hxr-io/gitlab-ci-operator/ci-python:0.1.0
  stage: test
  script:
    - black --version
    - black --check --diff --line-length 99 --target-version py38 ryo_iso
    - flake8 --version
    - flake8 ryo_iso
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event' && $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME !~ /^.*container\/.*$/

######## #### ####  ####   ####     ####        ####             ####
# Test - pytest
########

test-pytest:
  image: ubuntu:jammy
  stage: test
  script:
    - apt-get update
    - apt-get install --no-install-recommends --assume-yes python3 python3-pip ubuntu-keyring gpgv2 git
    - python3 -m pip install pytest pytest-cov tox
    - python3 -m pip install .
    - tox
  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
  artifacts:
    when: always
    paths:
      - coverage.xml
      - coverage
    reports:
      junit: report.xml
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
    expire_in: 1 week
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH

######## #### ####  ####   ####     ####        ####             ####
# Code Quality
########

include:
  - template: Code-Quality.gitlab-ci.yml

code_quality:
  after_script:
    - echo Completed.
  artifacts:
    paths: [gl-code-quality-report.json]
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      needs: [test-pytest]
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      needs: [test-pytest]

code_quality_html:
  extends: code_quality
  variables:
    REPORT_FORMAT: html
  after_script:
    - ls
    - echo Completed.
  artifacts:
    paths:
      - gl-code-quality-report.html
  needs: [code_quality]
  dependencies: [code_quality]
