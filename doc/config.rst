Configuration
=============

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   project_config
   general_config
