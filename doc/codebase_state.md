# Codebase State
- [x] Licenses
    * [x] reuse compliant
- [ ] .editorconfig
- [x] Changelog
- [x] Static Analysis
    * [x] Code formatting
    * [x] Lint
    * [ ] Cyclomatic complexity
- [ ] Dynamic Analysis
- [x] Unit tests
- [x] Integration tests
- [x] Coverage report generation
- [x] Code quality report
- [x] Online documentation
- [ ] Release management
- [x] Packaging
    * [ ] Ubuntu package
    * [x] PYPI package
- [ ] Test deployed package
