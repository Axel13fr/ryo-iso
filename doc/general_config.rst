General Configuration
=====================

Environment Variables
---------------------

While the defaults should work well for most applications, it may be useful to
to be able to set environmental variables for containerized builds.

.. envvar:: RYO_USER_CONFIG_DIR

    Path to directory containing ``config.yml``

.. envvar:: RYO_ISO_CONF

    Path to ``iso.yml``

.. envvar:: RYO_BUILD_DIR

    Build Directory


Program Configuration
---------------------

Operation of ``ryo-iso`` is configured by the parameters located in the first
config file found.

+---------------------------------------+
| Config File Search Order              |
+=======================================+
| ${RYO_USER_CONFIG_DIR}/config.yml     |
+---------------------------------------+
| ${XDG_CONFIG_HOME}/ryo-iso/config.yml |
+---------------------------------------+
| ~/.config/ryo-iso/config.yml          |
+---------------------------------------+

.. literalinclude:: config.yml
   :caption: ~/.config/ryo-iso/config.yml
   :name: config.yml
   :language: yaml

Parameters
``````````

================= ============ ===========================================
Parameter         Default      Description
================= ============ ===========================================
distro_image_dir  ~/Downloads  Download directory for upstream ISO images
progress          True         Display progress indicator
debug             True         Set 'DEBUG' logger level
================= ============ ===========================================


Project Defaults
----------------

Project defaults can be set for all projects via the first config found.

+-----------------------------------------+
| Project Defaults File Search Order      |
+=========================================+
| ${RYO_USER_CONFIG_DIR}/iso_base.yml     |
+-----------------------------------------+
| ${XDG_CONFIG_HOME}/ryo-iso/iso_base.yml |
+-----------------------------------------+
| ~/.config/ryo-iso/iso_base.yml          |
+-----------------------------------------+

.. literalinclude:: iso_base.yml
   :caption: ~/.config/ryo-iso/iso_base.yml
   :name: iso_base.yml
   :language: yaml

Parameters
``````````

=============== ======================================= =================================
Parameter       Default                                 Description
=============== ======================================= =================================
pip['version']  3                                       pip version
pip['target']   /usr/local/lib/python3.8/dist-packages  Installation target directory
pip['cache']    root/.cache/pip                         Chrooted pip installation dir
name            RYO-ISO                                 Volume name of the generated ISO
=============== ======================================= =================================

.. admonition:: Note

    More detailed information about configuration processing can be found in the `source <_modules/ryo_iso/config.html>`_.
