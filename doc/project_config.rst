Project Configuration
=====================

Overview
--------

To initialize the config files and start a new project.

.. code-block:: bash

    mkdir ~/iso_test
    cd ~/iso_test
    ryo-iso init

.. admonition:: Note

    By default, ``ryo-iso`` will load ``iso.yml`` from the current directory.
    This can be overridden by setting :envvar:`RYO_ISO_CONF` to location of the
    config file.

Project Configuration File
--------------------------

.. literalinclude:: iso.yml
   :caption: iso.yml
   :name: iso.yml
   :language: yaml

Parameters
``````````

+---------------------+-------------------------------------------------------+
| Parameter           | Description                                           |
+=====================+=======================================================+
| image               | The *distro*/*version* of the upstream ISO image      |
|                     | The version can be specified as the codename, or in   |
|                     | yy.mm or yy.mm.patch for Ubuntu                       |
+---------------------+-------------------------------------------------------+
| arch                | Target CPU architecture ['amd64','i386']              |
+---------------------+-------------------------------------------------------+
| variant             | Upstream ISO variant ['desktop', 'live-server']       |
+---------------------+-------------------------------------------------------+
| name                | Volume name of the generated ISO                      |
+---------------------+-------------------------------------------------------+
| apt['repository']   | List of apt repositories to add. Internally [#]_ this |
|                     | calls ``add-apt-repository`` within the chroot and    |
|                     | supports PPAs and custom repositories.                |
+---------------------+-------------------------------------------------------+
| apt['purge']        | List of packages to ``apt-get purge``                 |
+---------------------+-------------------------------------------------------+
| apt['install']      | List of packages to install via apt                   |
+---------------------+-------------------------------------------------------+
| dpkg['install']     | List of packages install via ``dpkg -i``              |
+---------------------+-------------------------------------------------------+
| pip['install']      | List of packages installed via pip                    |
+---------------------+-------------------------------------------------------+
| pip['local']        | List of Python Wheels to install via pip              |
|                     | :envvar:`RYO_BUILD_DIR`/python-wheels/                |
+---------------------+-------------------------------------------------------+
| patch               | Script to be run for final local customizations       |
+---------------------+-------------------------------------------------------+
| qemu['disk_size']   | Disk size for VM testing                              |
+---------------------+-------------------------------------------------------+
| qemu['usb-host']    | USB pass-through for VM testing                       |
+---------------------+-------------------------------------------------------+

Examples
````````

Image
:::::

.. code-block:: yaml

    # Image by codename
    image: ubuntu/jammy

    # Image yy.mm
    image: ubuntu/22.04

    # Image yy.mm.patch
    image: ubuntu/22.04.1

apt['repository']
:::::::::::::::::

.. code-block:: yaml

    apt:
      repository:
        # Copy sources.list from ${RYO_BUILD_DIR} into chrooted
        # /etc/apt/sourced.list
        # NOTE: This file is restored to the upstream version before building the ISO
        - sources.list
        # Copy ${RYO_BUILD_DIR}/sources.list.d/ros-latest.list into
        # /etc/apt/sources.list.d
        - sources.list.d/ros-latest.list
        # Install PPA and repository key
        - ppa:hxr-io/turtlebot

    apt:
      repository:
        - universe
        - multiverse

    apt:
      repository:
        - deb [arch=amd64 signed-by=/usr/share/keyrings/repo-keyring.gpg] http://repo.example.org/ubuntu jammy main
        - deb-src [arch=amd64 signed-by=/usr/share/keyrings/repo-keyring.gpg] http://example.org/ubuntu jammy main

patch
:::::

.. code-block:: yaml

    patch: setup_lang.bash

.. code-block:: bash
   :caption: setup_lang.bash
   :name: patch

    #!/bin/bash
    cat << EOF | sudo chroot squashfs-root
    check-language-support
    apt install $(check-language-support)
    EOF


Server Configuration File
:::::::::::::::::::::::::

.. literalinclude:: iso_server.yml
   :caption: iso.yml
   :name: Server ISO Config
   :language: yaml

Footnotes
`````````
.. [#] See :func:`ryo_iso.tasks.main.task__squashfs_apt_repo`
